# Lập trình web cuối kỳ nhóm 8
## Đề tài: Quản lý điểm sinh viên

### Thành viên
| Mã sinh viên | Họ và tên | Branch | Nhiệm vụ |
| :---: | :--- | :--- | :--- |
| 19000496 | Đỗ Bảo Trung | auth |Login, logout |
| 19000466 | Dương Văn Quang || Màn hình home..(phân quyền) |
| 19000385 | Dương Tùng Anh || Reminder password/Cấp lại password |
| 19000387 | Lương Xuân Anh || Tìm kiếm, xem chi tiết thông tin giáo viên |
| 19000390 | Nguyễn Thị Hải Anh || Thêm mới giáo viên |
| 19000389 | Nguyễn Tuấn Anh || Sửa thông tin giáo viên |
| 19000393 | Hoàng Thị Ngọc Ánh  || Tìm kiếm, xem chi tiết,xóa thông tin về sinh viên |
| 19000405 | Công Anh Dũng  || Thêm sinh viên |
| 19000406 | Đỗ Năng Dũng (bỏ nhóm) || Sửa thông tin sinh viên |
| 19000408 | Nguyễn Tùng Dương || Tìm kiếm, xóa môn học |
| 19000414 | Nguyễn Minh Đức || Sửa/xóa môn học |
| 19000420 | Hoàng Đức Hải || Tìm kiếm/filter điểm theo sinh viên OR theo môn học |
| 19000427 | Lê Thị Hoa || Tìm kiếm nâng cao |
| 19000438 | Phạm Thị Huyền || Nhập/Sửa điểm cho sinh viên |